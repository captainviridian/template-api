const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

const { 
  statusCodes, 
  requiredFields,
  events,
  db: dbConstants,
  USERS_SUBJECT_NAME ,
} = require('./constants');

const regexes = {
  // from http://www.regular-expressions.info/email.html
  email: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  password: /([A-zÁ-ú]|[0-9]){8,32}/,
};


module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const usersCollection = mongoClient
    .db(dbConstants.DB_NAME)
    .collection(dbConstants.USERS_COLLECTION_NAME);

  const createUserValidations = {
    name: ({ name }) => (typeof name === "string" && name.length > 0),

    email: async ({ email }) => {
      if(typeof email === "string" && regexes.email.test(email)) {
        const user = await usersCollection.findOne({ email });
        return !user;
      } else return false;
    },

    password: ({ password }) => (typeof password === "string" && regexes.password.test(password)),
  };
  
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", async (req, res) => {
    const { body } = req;

    for (const fieldName of requiredFields) {
      if(!body[fieldName]) {
        const error = `Request body had missing field ${fieldName}`;
        return res.status(statusCodes.BAD_REQUEST).json({ error });
      }
    }

    for (const [fieldName, validate] of Object.entries(createUserValidations)) {
      const isValid = await validate(body);
      if(!isValid) {
        const error = `Request body had malformed field ${fieldName}`;
        return res.status(statusCodes.BAD_REQUEST).json({ error });
      }
    }

    const { name, email, password, passwordConfirmation } = body;
    if(password !== passwordConfirmation)
      return res.status(statusCodes.UNPROCESSABLE_ENTITY).json({ error: "Password confirmation did not match" });

    const userId = uuid();

    const userCreatedMessage = {
      entityType: events.USER_CREATED,
      entityId: userId,
      entityAggregate: {
        name,
        email,
        password,
      },
    };
    stanConn.publish(USERS_SUBJECT_NAME, JSON.stringify(userCreatedMessage));

    res.status(statusCodes.CREATED).json({
      user: {
        id: userId,
        name,
        email,
      },
    });
  });

  api.delete("/users/:id", (req, res) => {
    const authenticationHeader = req.get("Authentication");
    const [schema, token] = (authenticationHeader && authenticationHeader.split(" ")) || [];
    
    if(!authenticationHeader || !schema || !token || schema.toLowerCase() !== "bearer") 
      return res.status(statusCodes.UNAUTHORIZED).json({ error: "Access Token not found" });

    const { id: tokenId } = jwt.verify(token, secret);
    const { id } = req.params;

    if(id !== tokenId)
      return res.status(statusCodes.FORBIDDEN).send({ error: "Access Token did not match User ID" })
    
    const userDeletedMessage = {
      entityType: events.USER_DELETED,
      entityId: id,
      entityAggregate: {
      },
    };
    stanConn.publish(USERS_SUBJECT_NAME, JSON.stringify(userDeletedMessage));

    res.status(statusCodes.OK).json({ id });
  });

  return api;
};
