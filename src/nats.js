const stan = require("node-nats-streaming");
const { 
  events, 
  USERS_SUBJECT_NAME,
  db: dbConstants,
} = require("./constants");

module.exports = function (mongoClient) {
  const usersCollection = mongoClient
    .db(dbConstants.DB_NAME)
    .collection(dbConstants.USERS_COLLECTION_NAME);

  const eventHandlers = {
    [events.USER_CREATED]: async function({
      entityId: id,
      entityAggregate: {
        name,
        email,
        password,
      }
    }) {
      try {
        await usersCollection.insertOne({ id, name, email, password });
      } catch(e) {
        console.log("An error occured while inserting: ", e);
      }
    },
    [events.USER_DELETED]: async function({ entityId }) {
      console.log("Deleted user: ", entityId);
    },
  };

  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const subscription = conn.subscribe(USERS_SUBJECT_NAME);
    subscription.on("message", function(msg) {
      const data = JSON.parse(msg.getData());
      const handleEvent = eventHandlers[data.entityType];

      if(handleEvent) {
        handleEvent(data);
      } else {
        console.log("Unable to process event of type: ", data.entityType);
      }
    });

  });

  return conn;
};
