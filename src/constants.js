const statusCodes = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    UNPROCESSABLE_ENTITY: 422,
};

const userConstants = {
    NAME: "name",
    EMAIL: "email",
    PASSWORD: "password",
    PASSWORD_CONFIRMATION: "passwordConfirmation",
};

const db = {
    DB_NAME: 'Service',
    USERS_COLLECTION_NAME: 'Users',
}

const requiredFields = Object.values(userConstants);

const USERS_SUBJECT_NAME = "users";

const events = {
    USER_CREATED: "UserCreated",
    USER_DELETED: "UserDeleted",
};

module.exports = {
    statusCodes,
    userConstants,
    events,
    db,
    requiredFields,
    USERS_SUBJECT_NAME,
};
